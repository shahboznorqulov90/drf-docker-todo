from django.test import TestCase
from ..models.models import TaskList, ListAccess, Task

class TaskListTest(TestCase):
    """ Test module for Puppy model """

    def setUp(self):
        TaskList.objects.create(
            name='TaskList 1', description='TaskList 1 Description')
        TaskList.objects.create(
            name='', description='TaskList 2 Description')

    # def test_fails_without_name(self):
    #     TaskList.objects.create(
    #         name='', description='TaskList 2 Description')
    #     self.assertEqual(
    #         task_2.name, "Muffin belongs to Gradane breed.")

    def test_created_with_name(self):
        task_1 = TaskList.objects.get(name='TaskList 1')

        self.assertEqual(
            task_1.name, "TaskList 1")
