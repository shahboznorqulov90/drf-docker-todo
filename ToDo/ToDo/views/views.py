from ..authentication import ToDoTokenAuthentication
from rest_framework.views import APIView
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, AllowAny
from ..models.models import TaskList, ListAccess, Task
from ..models.mor_models import Coordinate, Place, MeasurementEquipmentConfiguration, FTPServer, MeasurementEquipment

class HelloWorld(APIView):
    # authentication_classes = (BasicAuthentication,)
    def get(self, request):
        return Response('HELLO WORLD! from Django.')

class ListAdd(APIView):
    authentication_classes = (ToDoTokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        if request.data.get('name', None) and request.data.get('name') != '':
            # Getting request data
            name = request.data.get('name')
            description = request.data.get('description') if request.data.get('description', None) else ''

            # Writing to database
            try:
                new_list = TaskList(name=name, description=description)
                new_list.save()
                new_list_access = ListAccess(user=request.user, list=new_list, role='owner')
                new_list_access.save()

                # Responding back
                resp_dict = {
                    'status': 'success',
                    'message': 'List created successfully',
                    'data': {'id': new_list.id, 'name': new_list.name, 'description': new_list.description}
                }
                resp = Response()
                resp.status_code = 201
                resp.data = resp_dict
            except ValueError as val_err:
                # Responding back
                resp_dict = {
                    'status': 'failed',
                    'message': 'Something went wrong while writing to database, {0}'.format(val_err),
                    'data': {}
                }
                resp = Response()
                resp.status_code = 400
                resp.data = resp_dict
            except Exception as er:
                # Responding back
                resp_dict = {
                    'status': 'failed',
                    'message': 'Something unexpected happened!, {0}'.format(er),
                    'data': {}
                }
                resp = Response()
                resp.status_code = 400
                resp.data = resp_dict

        else:
            resp_dict = {
                'status': 'failed',
                'message': 'List name is required but not provided',
                'data': {}
            }
            resp = Response()
            resp.status_code = 400
            resp.data = resp_dict

        return resp

class ListFetch(APIView):
    authentication_classes = (ToDoTokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        resp_dict = {
            'status': '',
            'message': '',
            'data': None
        }

        try:
            list_ids = ListAccess.objects.values_list('list').filter(user=request.user)
            lists = TaskList.objects.filter(id__in=list_ids).values()
            resp_dict['status'] = 'Success'
            resp_dict['message'] = 'Retrieved the list of todo lists'
            resp_dict['data'] = lists

        except Exception as e:
            print(e)
            resp_dict['status'] = 'Failed'
            resp_dict['message'] = 'Something went wrong while fetching data. Error: '+e.__str__()
            resp_dict['data'] = None

        return Response(resp_dict)

class TaskAdd(APIView):
    authentication_classes = (ToDoTokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        resp_dict = {
            'status': None,
            'message': None,
            'data': None
        }

        req_list_id = request.data.get("list_id")
        req_task_name = request.data.get("name")
        req_task_desc = request.data.get('description') if request.data.get('description', None) else ''

        if req_list_id and TaskList.objects.filter(id=req_list_id).exists() and \
                req_task_name and req_task_name != '':
            try:
                task_list = TaskList.objects.get(id=req_list_id)

                user_perm = ListAccess.objects.filter(user=request.user, list=task_list)

                if user_perm.count() != 1 or user_perm.first().role != 'owner':
                    raise PermissionError("You do not have permission to edit this list")

                new_task = Task(name=req_task_name, list=task_list, description=req_task_desc)
                new_task.save()

                resp_dict['status'] = "success"
                resp_dict['message'] = "Task creation successful"
                resp_dict['data'] = {"name": new_task.name, "description": new_task.description, "done": new_task.done,
                                     "list_id": new_task.list.id}
                resp = Response(resp_dict)
                resp.status_code = 201

            except PermissionError as pe:
                resp_dict['status'] = "failed"
                resp_dict['message'] = pe.__str__()
                resp_dict['data'] = None
                resp = Response(resp_dict)
                resp.status_code = 403
            except Exception as e:
                resp_dict['status'] = "failed"
                resp_dict['message'] = "Something went wrong, Error: "+e.__str__()
                resp_dict['data'] = None
                resp = Response(resp_dict)
                resp.status_code = 500

        else:
            resp_dict['status'] = "failed"
            resp_dict['message'] = "Invalid name or list_id passed"
            resp_dict['data'] = None
            resp = Response(resp_dict)
            resp.status_code = 400

        return resp

class TaskFetch(APIView):
    authentication_classes = (ToDoTokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request):

        resp_dict = {
            'status': None,
            'message': None,
            'data': None
        }

        try:
            list_id = request.query_params.get("list_id", None)
            # checking if the list id is provided
            if list_id is None or list_id == '':
                raise ValueError("Invalid list_id")

            # fetching list object
            try:
                task_list_obj = TaskList.objects.get(id=list_id)
            except ObjectDoesNotExist:
                raise ValueError("Invalid list_id")

            # checking if the user has permission on the given list
            try:
                list_perm_qs = ListAccess.objects.get(user=request.user, list=task_list_obj)
            except ObjectDoesNotExist:
                raise PermissionError("You do not have permission to access this list")

            # fetching tasks
            tasks = Task.objects.filter(list=task_list_obj).values()

            resp_dict['status'] = "success"
            resp_dict['message'] = "Fetched tasks successfully"
            resp_dict['data'] = tasks
            resp = Response(resp_dict)
            resp.status_code = 200

        except PermissionError as pe:
            resp_dict['status'] = "failed"
            resp_dict['message'] = pe.__str__()
            resp_dict['data'] = None
            resp = Response(resp_dict)
            resp.status_code = 403
        except ValueError as ve:
            resp_dict['status'] = "failed"
            resp_dict['message'] = ve.__str__()
            resp_dict['data'] = None
            resp = Response(resp_dict)
            resp.status_code = 400
        except Exception as e:
            resp_dict['status'] = "failed"
            resp_dict['message'] = "Something went wrong, Error: " + e.__str__()
            resp_dict['data'] = None
            resp = Response(resp_dict)
            resp.status_code = 500

        return resp
