from ..authentication import ToDoTokenAuthentication
from rest_framework.views import APIView
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.viewsets import ModelViewSet
from ..models.mor_models import ( Coordinate, Place, MeasurementEquipmentConfiguration, FTPServer, MeasurementEquipment,
MeasurementResult, InversionConfiguration, InversionResult, ImageConfiguration, ImageResult )
from ..serializers.mor_serializers import ( CoordinateSerializer, PlaceSerializer, MeasurementEquipmentConfigurationSerializer,
FTPServerSerializer, MeasurementEquipmentSerializer, MeasurementResultSerializer, InversionConfigurationSerializer,
InversionResultSerializer, ImageConfigurationSerializer, ImageResultSerializer)

class CoordinateViewSet(ModelViewSet):
    """
    A simple ViewSet for viewing and editing the coordinate model objects.
    """
    serializer_class = CoordinateSerializer
    authentication_classes = (ToDoTokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return Coordinate.objects.all()

class PlaceViewSet(ModelViewSet):
    """
    A simple ViewSet for viewing and editing the coordinate model objects.
    """
    serializer_class = PlaceSerializer
    authentication_classes = (ToDoTokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return Place.objects.all()

class MeasurementEquipmentConfigurationViewSet(ModelViewSet):
    """
    A simple ViewSet for viewing and editing the coordinate model objects.
    """
    serializer_class = MeasurementEquipmentConfigurationSerializer
    authentication_classes = (ToDoTokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return MeasurementEquipmentConfiguration.objects.all()

class FTPServerViewSet(ModelViewSet):
    """
    A simple ViewSet for viewing and editing the coordinate model objects.
    """
    serializer_class = FTPServerSerializer
    authentication_classes = (ToDoTokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return FTPServer.objects.all()

class MeasurementEquipmentViewSet(ModelViewSet):
    """
    A simple ViewSet for viewing and editing the coordinate model objects.
    """
    serializer_class = MeasurementEquipmentSerializer
    authentication_classes = (ToDoTokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return MeasurementEquipment.objects.all()

class MeasurementResultViewSet(ModelViewSet):
    """
    A simple ViewSet for viewing and editing the coordinate model objects.
    """
    serializer_class = MeasurementResultSerializer
    authentication_classes = (ToDoTokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        queryset =  MeasurementResult.objects.all()
        equipment = self.request.query_params.get('equipment', None)
        if equipment is not None:
            queryset = queryset.filter(equipment=equipment)
        return queryset


class InversionConfigurationViewSet(ModelViewSet):
    """
    A simple ViewSet for viewing and editing the coordinate model objects.
    """
    serializer_class = InversionConfigurationSerializer
    authentication_classes = (ToDoTokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return InversionConfiguration.objects.all()

class InversionResultViewSet(ModelViewSet):
    """
    A simple ViewSet for viewing and editing the coordinate model objects.
    """
    serializer_class = InversionResultSerializer
    authentication_classes = (ToDoTokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return InversionResult.objects.all()

class ImageConfigurationViewSet(ModelViewSet):
    """
    A simple ViewSet for viewing and editing the coordinate model objects.
    """
    serializer_class = ImageConfigurationSerializer
    authentication_classes = (ToDoTokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return ImageConfiguration.objects.all()

class ImageResultViewSet(ModelViewSet):
    """
    A simple ViewSet for viewing and editing the coordinate model objects.
    """
    serializer_class = ImageResultSerializer
    authentication_classes = (ToDoTokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return ImageResult.objects.all()
