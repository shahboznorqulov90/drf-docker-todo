from rest_framework.serializers import ModelSerializer
from ..models.mor_models import ( Coordinate, Place, MeasurementEquipmentConfiguration, FTPServer, MeasurementEquipment,
MeasurementResult, InversionConfiguration, InversionResult, ImageConfiguration, ImageResult )

class CoordinateSerializer(ModelSerializer):
    class Meta:
        model = Coordinate
        fields = '__all__'



class MeasurementEquipmentConfigurationSerializer(ModelSerializer):
    class Meta:
        model = MeasurementEquipmentConfiguration
        fields = '__all__'

class FTPServerSerializer(ModelSerializer):
    class Meta:
        model = FTPServer
        fields = '__all__'

class MeasurementEquipmentSerializer(ModelSerializer):
    class Meta:
        model = MeasurementEquipment
        fields = '__all__'

class PlaceSerializer(ModelSerializer):
    equipments = MeasurementEquipmentSerializer(many=True, required=False, allow_null=True)

    class Meta:
        model = Place
        fields = '__all__'

class MeasurementResultSerializer(ModelSerializer):
    class Meta:
        model = MeasurementResult
        fields = '__all__'

class InversionConfigurationSerializer(ModelSerializer):
    class Meta:
        model = InversionConfiguration
        fields = '__all__'

class InversionResultSerializer(ModelSerializer):
    class Meta:
        model = InversionResult
        fields = '__all__'

class ImageConfigurationSerializer(ModelSerializer):
    class Meta:
        model = ImageConfiguration
        fields = '__all__'

class ImageResultSerializer(ModelSerializer):
    class Meta:
        model = ImageResult
        fields = '__all__'
