from django.db import models
from django.conf import settings
from django.contrib import admin

class Coordinate(models.Model):

    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    latitude = models. DecimalField(max_digits=20, decimal_places=17)
    longitude = models. DecimalField(max_digits=20, decimal_places=17)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return 'coordinate {self.name}'.format(self=self)

class CoordinateAdmin(admin.ModelAdmin):
    pass

admin.site.register(Coordinate, CoordinateAdmin)

class Place(models.Model):

    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    coordinate = models.ForeignKey('Coordinate', on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return 'place {self.name}'.format(self=self)

class PlaceAdmin(admin.ModelAdmin):
    pass

admin.site.register(Place, PlaceAdmin)

class MeasurementEquipmentConfiguration(models.Model):

    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    data = models.JSONField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return 'm_e_config {self.name}'.format(self=self)

class MeasurementEquipmentConfigurationAdmin(admin.ModelAdmin):
    pass

admin.site.register(MeasurementEquipmentConfiguration, MeasurementEquipmentConfigurationAdmin)

class FTPServer(models.Model):

    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    url = models.URLField()
    username = models.CharField(max_length=50)
    password = models.CharField(max_length=50)
    measurements_path = models.CharField(max_length=200)
    configurations_path = models.CharField(max_length=200)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return 'ftp_server {self.name}'.format(self=self)

class FTPServerAdmin(admin.ModelAdmin):
    pass

admin.site.register(FTPServer, FTPServerAdmin)

class MeasurementEquipment(models.Model):

    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    place = models.ForeignKey('Place', on_delete=models.CASCADE, related_name='equipments')
    ftp_server = models.ForeignKey('FTPServer', on_delete=models.CASCADE)
    configuration = models.ForeignKey('MeasurementEquipmentConfiguration', on_delete=models.CASCADE)
    specifications_data = models.JSONField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return 'm_equipment {self.name}'.format(self=self)

class MeasurementEquipmentAdmin(admin.ModelAdmin):
    pass

admin.site.register(MeasurementEquipment, MeasurementEquipmentAdmin)

class MeasurementResult(models.Model):

    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    equipment = models.ForeignKey('MeasurementEquipment', on_delete=models.CASCADE)
    configuration = models.ForeignKey('MeasurementEquipmentConfiguration', on_delete=models.CASCADE)
    date_time = models.DateTimeField()
    file = models.FileField(upload_to='measurement_results', blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return 'mesurement_result {self.name}'.format(self=self)

class MeasurementResultAdmin(admin.ModelAdmin):
    pass

admin.site.register(MeasurementResult, MeasurementResultAdmin)


class InversionConfiguration(models.Model):

    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    data = models.JSONField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return 'inv_config {self.name}'.format(self=self)

class InversionConfigurationAdmin(admin.ModelAdmin):
    pass

admin.site.register(InversionConfiguration, InversionConfigurationAdmin)

class InversionResult(models.Model):

    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    result = models.ForeignKey('MeasurementResult', on_delete=models.CASCADE)
    configuration = models.ForeignKey('InversionConfiguration', on_delete=models.CASCADE)
    file = models.FileField(upload_to='inversion_results', blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return 'inv_result {self.name}'.format(self=self)

class InversionResultAdmin(admin.ModelAdmin):
    pass

admin.site.register(InversionResult, InversionResultAdmin)

class ImageConfiguration(models.Model):

    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    data = models.JSONField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return 'image_config {self.name}'.format(self=self)

class ImageConfigurationAdmin(admin.ModelAdmin):
    pass

admin.site.register(ImageConfiguration, ImageConfigurationAdmin)

class ImageResult(models.Model):

    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    inversion_result = models.ForeignKey('InversionResult', on_delete=models.CASCADE)
    configuration = models.ForeignKey('ImageConfiguration', on_delete=models.CASCADE)
    file = models.FileField(upload_to='image_results', blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return 'image {self.name}'.format(self=self)

class ImageResultAdmin(admin.ModelAdmin):
    pass

admin.site.register(ImageResult, ImageResultAdmin)
